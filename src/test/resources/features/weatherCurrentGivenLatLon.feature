Feature: To Validate Weather API response to GET Current Weather update  given latitude and longitude

@smoke @regression @weatherCurrentGivenLatLon
  Scenario Outline: GET Current Weather update  given latitude and longitude and Validate State code
    Given Latitude "<lat>" and longitude "<lon>" of city where key "<key>"
    Then response status code should be <statuscode>
    And response should include the city name as "<state_code>" and weather description
    
    Examples:
      | lat       | lon        | key                              |  statuscode | state_code |
      | 40.730610 | -73.935242 | 453017126cd04cf78d9b3076c728b7a5 |  200        | NY         |
      | 38        | -78.25     | 453017126cd04cf78d9b3076c728b7a5 |  200        | VA         |

 @regression
   Scenario Outline:  Error scenarios given invalid values of latitude , longitude, key, validate error status code
    Given Latitude "<lat>" and longitude "<lon>" of city where key "<key>"
    Then response status code should be <statuscode>
   
    Examples:
    | lat        | lon           | key                              | statuscode |
    | 4a.7       | -73.935242    | 453017126cd04cf78d9b3076c728b7a5 |  400       |
    | 38         | -kl.25        | 453017126cd04cf78d9b3076c728b7a5 |  400       |
    | 40.730610  | -73.935242    | test                             |  403       |