Feature:  To Validate Weather API response to get Hourly forecast based on passing an Postal code 

@smoke @regression @weatherHourlyGivenPostcode
 Scenario Outline: GET 3 Hourly forecast based on passing an Postal code validate timestamp_utc and weather
    Given Postcode "<postcode>" for a location and key "<key>"
    Then the response status code should be <statuscode>
    And response should contain timestamp utc and weather description
  
   Examples:
    | postcode | key                              | statuscode |
    | 28456    | 453017126cd04cf78d9b3076c728b7a5 |  200       |
    | 2147     | 453017126cd04cf78d9b3076c728b7a5 |  200       |
   
 @regression
Scenario Outline: Given invalid values of postalcode,key validate error status code 
    Given Postcode "<postcode>" for a location and key "<key>"
    Then the response status code should be <statuscode>
  
   Examples:
    | postal_code  | key                              | statuscode |
    | qwerty       | 453017126cd04cf78d9b3076c728b7a5 |   204      |
    | 28456        | test                             |   403      |
   
