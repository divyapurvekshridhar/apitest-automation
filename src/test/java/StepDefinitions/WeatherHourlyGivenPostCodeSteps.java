package StepDefinitions;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class WeatherHourlyGivenPostCodeSteps {
	Response getWeatherResponse;
	@Given("^Postcode \"([^\"]*)\" for a location and key \"([^\"]*)\"$")
	public void postcode_for_a_location_and_key(String postcode, String key) {
		RestAssured.baseURI="http://api.weatherbit.io/v2.0";
		getWeatherResponse = given().
							queryParam("postal_code", postcode).
							queryParam("key", key).
					  when().
					         get("/forecast/3hourly").
					  then().
					         extract().response();
		
	}

	@Then("^the response status code should be (\\d+)$")
	public void the_response_status_code_should_be(int statuscode) {
		int actualStatusCode = getWeatherResponse.getStatusCode();
		System.out.println(actualStatusCode);
	    Assert.assertEquals(statuscode, actualStatusCode);
	}
	

	@Then("^response should contain timestamp utc and weather description$")
	public void response_should_contain_timestamp_utc_and_weather_description()  {
		String getWeatherResponseValue = getWeatherResponse.asString();  
		System.out.println(getWeatherResponseValue);
    	JsonPath js= new JsonPath(getWeatherResponseValue);
    	
    	int count = js.get("data.size()");
    	System.out.println(count);
    	for(int i=0; i<count; i++) {
    		
    	//To get timestamp utc for each hour and validate
        String timeStampUtc = js.get("data["+i+"].timestamp_utc");
        System.out.println(timeStampUtc);
        Assert.assertFalse(timeStampUtc.isEmpty());	
    		
    	//To get weather description for each hour and validate
        String weatherDescription = js.get("data["+i+"].weather.description");
        System.out.println(weatherDescription);
    	Assert.assertFalse(weatherDescription.isEmpty());
    	}
	}
}
