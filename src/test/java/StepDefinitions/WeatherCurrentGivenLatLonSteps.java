package StepDefinitions;

import static io.restassured.RestAssured.given;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class WeatherCurrentGivenLatLonSteps {
	Response getResponse;
	@Given("^Latitude \"([^\"]*)\" and longitude \"([^\"]*)\" of city where key \"([^\"]*)\"$")
	public void latitude_and_longitude_of_city_where_key(String lat, String lon, String key)  {
		RestAssured.baseURI="http://api.weatherbit.io/v2.0";
			
		getResponse = given().
							queryParam("lat", lat).
							queryParam("lon", lon).
							queryParam("key", key).
					  when().
					         get("/current").
					  then().
					         assertThat().contentType(ContentType.JSON).extract().response();
		
	}

	@Then("^response status code should be (\\d+)$")
	public void response_status_code_should_be(int statuscode) {
	int actualStatusCode = getResponse.getStatusCode();
	System.out.println(actualStatusCode);
    Assert.assertEquals(statuscode, actualStatusCode);
	}

	@Then("^response should include the city name as \"([^\"]*)\" and weather description$")
	public void response_should_include_the_city_name_as_and_weather_description(String statecode) {
		String getWeatherResponseValue = getResponse.asString();  
		System.out.println(getWeatherResponseValue);
    	JsonPath js= new JsonPath(getWeatherResponseValue);
    	
    	//To validate the state code
    	String actualStateCode = js.get("data[0].state_code");	
    	System.out.println(actualStateCode);	
    	Assert.assertTrue(actualStateCode.equals(statecode));
    	
    	//To validate the weather description
    	String weatherDescription = js.get("data[0].weather.description");
    	System.out.println(weatherDescription);
    	Assert.assertFalse(weatherDescription.isEmpty());
	}
}
