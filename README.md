API Test Framework
=====================================================
The Tech Stack used is as below

1. Java

2. Rest Assured

3. Cucumber for BDD

4. JUnit


Pre-requisites to be set up on the machine
====================================================
1.Java

2.Maven

3.IDE like Eclipse/IntelliJ

How to execute the Test
=====================================================
1. Download/Clone the repo on to the local machine

2. Ensure Java and Maven have been installed on the system

3. Go into the repo folder[where pom.xml resides] and execute the command 'mvn clean install' to download maven dependencies and clean   the target

4. Execute the Maven tests using the command 'mvn test'

5. The execution will happen and Report will be available in the target folder/ cucumber-reports /index.html

6. We can also use editor IDE like Eclipse/IntelliJ where we can import the project as a Maven project and run tests


Execute test scenarios on the command line
=========================================================
We can execute a subset of tests using tags

a. To run smoke tests(Scenarios with tag @smoke)

  mvn test  -Dcucumber.options="--tags @smoke"

b. To run regression tests(Scenarios with tag @regression)

 mvn test  -Dcucumber.options="--tags @regression"

Test Scenarios
=======================================================
1. @weatherCurrentGivenLatLon :  To Validate Weather API response to GET Current Weather update  given latitude and longitude

2. @weatherHourlyGivenPostcode: To Validate Weather API response to get Hourly forecast based on passing an Postal code 

==================================================================================

Note: The API Automation technical test mentions to "GET 3 Hourly forecast based on passing an Australian Postal code"
 
But we are NOT ABLE to access the endpoint for the free version. 

Please refer to below link for clarification
https://www.weatherbit.io/pricing

Hence the tests below will fail with error code of 403 Forbidden
 
Scenario Outline:  GET 3 Hourly forecast based on passing an Postal code validate timestamp_utc and weather